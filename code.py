import requests
import json
from datetime import date, timedelta
import operator
import logging
import time
from string import ascii_lowercase
import random

ALPHAVANTAGE_BASE_URL = 'https://www.alphavantage.co/query?function'
logger = logging.getLogger()


def get_api():
    """
    API key pool for random shuffling keys to run into less API limit errors
    """
    api_pool = [
        '7Y9XI9DTQT3PGWBM',
        '2FB3KSEPKBOQ02D3',
        'ZZ7ILDYBBBH7HAT5',
        '9MNEEMEO6X2E8WAL',
        'AP8QDE81SRSAZ3FU',
        '83H8B3XFF5EKMHL5',
    ]

    ix = random.randint(0, 5)
    return api_pool[ix]


def get_symbol_list(n: int, sec: float=1.5) -> list:
    """
    loop a-z to form a list of 100 stock tickers
    :return: a list of stock symbols
    """
    # return ['A', 'ACBFF', 'AAPL', 'AMZN', 'AMD', 'BABA', 'ACRX', 'EARS', 'AMRN', 'T', 'B', 'BAC', 'BABA', 'BE', 'BJ',
    #         'BV', '002933.SHZ', 'BSVN', 'BRY', '6190.HKG', 'C', 'CGC', 'CRON', 'CWK', 'CTK', 'CNST', 'CBNK', 'CANG',
    #         'CRNX', 'CRN.AUS', 'D', 'DAVA', 'DOMO', 'DDMXU', '6111.HKG', 'DDOC', 'DIS', 'NUGT', 'JNUG', 'E', 'EARS',
    #         'ESTC', 'EB', 'ELAN', 'DAVA', 'EVER', 'EQ', 'ETTX', 'ESTA', 'F', 'FB', 'FTCH', 'MYFW', 'FILM.JKT', 'FOCS',
    #         'FTSV', '1721.HKG', 'FLKS', 'FCX']

    symbol_list = []

    def search_symbol_by_letter(letter: str) -> list:
        time.sleep(sec)
        api_endpoint = "{}=SYMBOL_SEARCH&keywords={}&apikey={}".format(ALPHAVANTAGE_BASE_URL, letter, get_api())
        response = requests.get(api_endpoint)
        bestMatches = json.loads(response.text)['bestMatches']
        print(bestMatches)
        symbols = [i['1. symbol'] for i in bestMatches]
        print(symbols)
        return symbols

    for l in ascii_lowercase:
        if len(symbol_list) <= n:
            print(len(symbol_list))
            print(symbol_list)
            newSeach = search_symbol_by_letter(l)
            symbol_list += newSeach

    return symbol_list


def get_vol_d_1(quote: str, sec: float, retry_sec: float) -> float:
    """
    :param quote: stock symbol
    :param sec: default 0.5 seconds to delay the API call per symbol
    :return: absolute value of price change in terms of float
    """

    def get_quote(api_key: str, qry_url: str) -> dict:
        print(api_key, quote)
        api_endpoint = "{}=TIME_SERIES_DAILY&symbol={}&apikey={}".format(qry_url, quote, api_key)
        response = requests.get(api_endpoint)
        return json.loads(response.text)

    time.sleep(sec)
    quote_dict = get_quote(get_api(), ALPHAVANTAGE_BASE_URL)

    yesterday = (date.today() - timedelta(1)).strftime('%Y-%m-%d')
    try:
        d_1_object = quote_dict['Time Series (Daily)'][yesterday]
        d_open = float(d_1_object['1. open'])
        d_close = float(d_1_object['4. close'])
        vol = abs((d_close - d_open) / d_open)
        print(vol)
        return vol
    except:
        logger.warning("Exceeding API limit, Retry in {} secs.".format(retry_sec))
        try:
            time.sleep(retry_sec)
            d_1_object = quote_dict['Time Series (Daily)'][yesterday]
            d_open = float(d_1_object['1. open'])
            d_close = float(d_1_object['4. close'])
            vol = abs((d_close - d_open) / d_open)
            return vol
        except:
            logger.error("Exceeding API limit, Retry failed.")


def construct_vol_obj(symbols: list, sec: float = 1.01, retry_sec: float = 5) -> dict:
    """
    :return: a dict of stock symbol and its yesterday price change
    """
    vol_obj = {k: get_vol_d_1(k, sec, retry_sec) for k in symbols}
    return vol_obj


def get_topN(n: int, vol_obj: dict) -> list:
    """
    :return: a list of tuples of stock symbol and price change
    """
    sorted_d = sorted(vol_obj.items(), key=operator.itemgetter(1), reverse=True)
    topN = sorted_d[:n]
    return topN


def reformat_topN(topn: list) -> list:
    return [(i[0], "{:.2%}".format(i[1])) for i in topn]


if __name__ == '__main__':
    symbol_list = get_symbol_list(50, sec=1.5)
    vol_obj = construct_vol_obj(symbol_list, sec=1.1, retry_sec=5)
    top10 = get_topN(10, vol_obj)
    result = reformat_topN(top10)
    print(result)
